package crawler

type Site struct {
	Identifier string
	Parser     ISiteParser
	Storage    ISiteStorage
}

type Feature struct {
	Identifier string
	Parser     IFeatureParser
	Storage    IFeatureStorage
}
