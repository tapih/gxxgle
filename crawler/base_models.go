package crawler

const URL_FIELD string = "url"
const IS_EXTRACTED_FIELD string = "is_link_extracted"

type IModel interface {
	GetUrl() string
}

// site
type ISiteModel interface {
	IModel
	GetIsLinkExtracted() bool
}

// TODO: unique violation occurs thoug data are all unique
type SiteModel struct {
	Url             string `gorm:"type:varchar(1024);unique_index"`
	IsLinkExtracted bool   `gorm:"default:false"`
}

func (m *SiteModel) GetUrl() string {
	return m.Url
}

func (m *SiteModel) GetIsLinkExtracted() bool {
	return m.IsLinkExtracted
}

// feature
type IFeatureModel interface {
	IModel
}

type FeatureModel struct {
	Url string `gorm:"type:varchar(1024);unique_index"`
}

func (m *FeatureModel) GetUrl() string {
	return m.Url
}
