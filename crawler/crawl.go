package crawler

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

// TODO: Loggerにzap使う
// TODO: 握りつぶしているエラーを一番上までバケツリレーする

type ResponseError struct {
	statusCode int
}

func (e *ResponseError) Error() string {
	return fmt.Sprintf("Received invalid status code: %d", e.statusCode)
}

func makeDocQuery(targetUrl string, locale string) (*goquery.Document, error) {
	req, err := http.NewRequest("GET", targetUrl, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept-language", locale)
	client := new(http.Client)
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != 200 {
		return nil, &ResponseError{res.StatusCode}
	}

	body := res.Body
	defer body.Close()
	doc, err := goquery.NewDocumentFromReader(body)
	if err != nil {
		return nil, err
	}
	return doc, nil
}

func saveSite(targetUrl string, doc *goquery.Document, site Site) bool {
	fea, err := site.Parser.Extract(doc, targetUrl)
	if err != nil {
		return false
	}
	isSaved, err := site.Storage.SaveIfNotExists(fea.GetUrl(), fea)
	if err != nil {
		return false
	}
	return isSaved
}

func saveFeatures(targetUrl string, doc *goquery.Document, feas []Feature) map[string]int {
	counts := make(map[string]int)

	for _, h := range feas {
		counts[h.Identifier] = 0
		features, err := h.Parser.Extract(doc, targetUrl)
		if err != nil {
			continue
		}

		for _, fea := range features {
			isSaved, err := h.Storage.SaveIfNotExists(fea.GetUrl(), fea)
			if err != nil {
				continue
			}
			if isSaved {
				counts[h.Identifier] += 1
			}
		}
	}
	return counts
}

func findNonExistingLinks(targetUrl string, doc *goquery.Document, site Site, ignoreHosts []string) []string {
	candidates := site.Parser.Find(doc, targetUrl)
	var targets []string
	for _, v := range candidates {
		parsed, err := url.Parse(v)
		if err != nil {
			continue
		}
		scheme := parsed.Scheme
		host := parsed.Host
		hostUrl := scheme + "://" + host
		if Contains(ignoreHosts, hostUrl) {
			continue
		}
		if Contains(targets, v) {
			continue
		}
		targets = append(targets, v)
	}
	return targets
}

func Crawl(
	targetUrl string,
	locale string,
	numUrls int,
	ignoreHosts []string,
	site Site,
	others []Feature,
	limitRoutine int,
	limitLoadOnce int,
	logLevel int,
) {
	var err error
	urlCounter := 0
	waitGroup := NewSizedWaitGroup(limitRoutine)

	var v string
	urls := []string{targetUrl}

	for flg := true; flg; {
		for len(urls) > 0 && flg {
			v, urls = urls[0], urls[1:]

			docRoot, err := makeDocQuery(v, locale)
			if err != nil {
				if logLevel > 2 {
					fmt.Printf("ERROR MAKEDOC %s %s\n", v, err)
				}
				continue
			}
			links := findNonExistingLinks(v, docRoot, site, ignoreHosts)
			fmt.Printf("INFO %s FOUND (%d records)\n", site.Identifier, len(links))

			for _, l := range links {
				waitGroup.Add()
				go func(linkUrl string) {
					defer waitGroup.Done()
					doc, err := makeDocQuery(linkUrl, locale)
					if err != nil {
						return
					}

					saved := saveSite(linkUrl, doc, site)
					if !saved {
						return
					}

					urlCounter++
					if logLevel > 0 {
						fmt.Printf("INFO %s SAVE (%dth record) %s\n", site.Identifier, urlCounter, linkUrl)
					}

					counts := saveFeatures(linkUrl, doc, others)
					for k, v := range counts {
						if logLevel > 1 {
							fmt.Printf("INFO %s SAVE (%d records) %s\n", k, v, targetUrl)
						}
					}
				}(l)

				if urlCounter >= numUrls {
					flg = false
					break
				}
			}
		}

		for repeat := 10; repeat > 0; repeat-- {
			urls, err = site.Storage.LoadLinksNotExtracted(limitLoadOnce)
			if err == nil {
				break
			}
			if repeat == 1 {
				panic("load failed")
			}
		}
		for repeat := 10; repeat > 0; repeat-- {
			err = site.Storage.TurnOnIsExtracted(urls)
			if err == nil {
				break
			}
			if repeat == 1 {
				panic("turn on failed")
			}
		}
	}
	waitGroup.Wait()
}
