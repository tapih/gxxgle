package crawler

import "testing"

var testNonRecursiveCase = [][]string{
	{"///fuga/fuga", "https://hogehoge.com", ""},
	{"////fuga/fuga", "https://hogehoge.com", ""},
	{".//fuga/fuga", "https://hogehoge.com", ""},
	{"..//fuga/fuga", "https://hogehoge.com", ""},
	{"#fuga", "https://hogehoge.com", ""},
	{"javascript:fuga", "https://hogehoge.com", ""},
	{"mailto:fuga@piyo", "https://hogehoge.com", ""},
	{"//fugafuga.com", "https://hogehoge.com", "https://fugafuga.com"},
	{"/fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{"/fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{"/fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/fuga/fuga"},
	{"/fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/fuga/fuga"},
	{"http://fugafuga.com", "https://hogehoge.com/piyo/", "http://fugafuga.com"},
	{"https://fugafuga.com", "https://hogehoge.com/piyo/", "https://fugafuga.com"},
	{"fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{"fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{"fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/piyo/fuga/fuga"},
	{"fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/piyo/fuga/fuga"},
}

var testRecursiveCase = [][]string{
	// ./
	{"./fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{"./fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{"./fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/piyo/fuga/fuga"},
	{"./fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/piyo/fuga/fuga"},
	// ../
	{"../fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{"../fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{"../fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/fuga/fuga"},
	{"../fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/fuga/fuga"},
	// ../../
	{"../../fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/piyo/piyo", "https://hogehoge.com/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/piyo/piyo/", "https://hogehoge.com/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/piyo/piyo/piyo", "https://hogehoge.com/piyo/fuga/fuga"},
	{"../../fuga/fuga", "https://hogehoge.com/piyo/piyo/piyo/piyo", "https://hogehoge.com/piyo/piyo/fuga/fuga"},
	// ./../
	{"./../fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{"./../fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{"./../fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/fuga/fuga"},
	{"./../fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/fuga/fuga"},
	{"./../fuga/fuga", "https://hogehoge.com/piyo/piyo", "https://hogehoge.com/piyo/fuga/fuga"},
	{"./../fuga/fuga", "https://hogehoge.com/piyo/piyo/", "https://hogehoge.com/piyo/fuga/fuga"},
	{"./../fuga/fuga", "https://hogehoge.com/piyo/piyo/piyo", "https://hogehoge.com/piyo/piyo/fuga/fuga"},
	// .././
	{".././fuga/fuga", "https://hogehoge.com", "https://hogehoge.com/fuga/fuga"},
	{".././fuga/fuga", "https://hogehoge.com/", "https://hogehoge.com/fuga/fuga"},
	{".././fuga/fuga", "https://hogehoge.com/piyo", "https://hogehoge.com/fuga/fuga"},
	{".././fuga/fuga", "https://hogehoge.com/piyo/", "https://hogehoge.com/fuga/fuga"},
	{".././fuga/fuga", "https://hogehoge.com/piyo/piyo/", "https://hogehoge.com/piyo/fuga/fuga"},
	{".././fuga/fuga", "https://hogehoge.com/piyo/piyo/piyo", "https://hogehoge.com/piyo/piyo/fuga/fuga"},
}

func TestMakeAbsoluteUrlNonRecursiveCase(t *testing.T) {
	for _, c := range testNonRecursiveCase {
		u, _ := MakeAbsoluteUrl(c[0], c[1])
		if u != c[2] {
			t.Fatalf("failed test: incorrect conversion (%s %s) -> %s", c[0], c[1], u)
		}
	}
}

func TestMakeAbsoluteUrlRecursiveCase(t *testing.T) {
	for _, c := range testRecursiveCase {
		u, _ := MakeAbsoluteUrl(c[0], c[1])
		if u != c[2] {
			t.Fatalf("failed test: incorrect conversion (%s %s) -> %s", c[0], c[1], u)
		}
	}
}
