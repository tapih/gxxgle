package crawler

import (
	"fmt"

	"github.com/BurntSushi/toml"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

type DbConfig struct {
	Db dbConfigCore
}

type dbConfigCore struct {
	Dist     string
	Host     string
	Port     int
	User     string
	Password string
	Dbname   string
}

func ConnectDb(path string) (*gorm.DB, error) {
	var config DbConfig
	_, err := toml.DecodeFile(path, &config)
	if err != nil {
		return nil, err
	}
	dbconfig := config.Db
	cnx := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		dbconfig.Host,
		dbconfig.Port,
		dbconfig.User,
		dbconfig.Password,
		dbconfig.Dbname,
	)
	db, err := gorm.Open(dbconfig.Dist, cnx)
	if err != nil {
		return nil, err
	}
	return db, nil
}
