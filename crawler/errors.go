package crawler

import "fmt"

type LengthMismatch struct {
	a int
	b int
}

func (e LengthMismatch) Error() string {
	return fmt.Sprintf("Lengths are not equal between %d and %d", e.a, e.b)
}
