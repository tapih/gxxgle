package crawler

import (
	"github.com/PuerkitoBio/goquery"
)

type ISiteParser interface {
	Find(*goquery.Document, string) []string
	Extract(*goquery.Document, string) (ISiteModel, error)
}

type IFeatureParser interface {
	Extract(*goquery.Document, string) ([]IFeatureModel, error)
}

type ParseAllAnchors struct{}

func (p *ParseAllAnchors) Find(doc *goquery.Document, url string) []string {
	var urls []string
	doc.Find("a").Each(func(_ int, s *goquery.Selection) {
		v, exists := s.Attr("href")
		if !exists {
			return
		}
		if u, err := MakeAbsoluteUrl(v, url); err == nil {
			// TODO append is slow...
			urls = append(urls, u)
		}
	})
	return urls
}
