package crawler

import (
	"fmt"
	"net/url"
	"path"
	"strings"
)

type InapropriateFormat struct {
	targetUrl string
	baseUrl   string
}

func (e InapropriateFormat) Error() string {
	return fmt.Sprintf(
		"Cannot make absolute url with \"%s\" and \"%s\"",
		e.targetUrl,
		e.baseUrl,
	)
}

func MakeAbsoluteUrl(targetUrl string, baseUrl string) (string, error) {
	if !strings.HasSuffix(baseUrl, "/") {
		baseUrl = baseUrl + "/"
	}
	parsed, _ := url.Parse(baseUrl)
	scheme := parsed.Scheme
	host := parsed.Host
	hostUrl := scheme + "://" + host
	root := path.Dir(parsed.Path)
	rootUrl := scheme + "://" + path.Join(host, root)

	switch {
	case strings.HasPrefix(targetUrl, "///") ||
		strings.HasPrefix(targetUrl, ".//") ||
		strings.HasPrefix(targetUrl, "..//") ||
		strings.HasPrefix(targetUrl, "#") ||
		strings.HasPrefix(targetUrl, "javascript:") ||
		strings.HasPrefix(targetUrl, "mailto:"):
		return "", InapropriateFormat{targetUrl, baseUrl}
	case strings.HasPrefix(targetUrl, "//"):
		return scheme + ":" + targetUrl, nil
	case strings.HasPrefix(targetUrl, "/"):
		return hostUrl + targetUrl, nil
	case strings.HasPrefix(targetUrl, "./"):
		if len(targetUrl) == 2 {
			return baseUrl, nil
		}
		return MakeAbsoluteUrl(targetUrl[2:], rootUrl)
	case strings.HasPrefix(targetUrl, "../"):
		parent := path.Dir(root)
		parentUrl := scheme + "://" + path.Join(host, parent)
		if len(targetUrl) == 3 {
			return parentUrl, nil
		}
		return MakeAbsoluteUrl(targetUrl[3:], parentUrl)
	case strings.HasPrefix(targetUrl, "http://") || strings.HasPrefix(targetUrl, "https://"):
		return targetUrl, nil
	default:
		return rootUrl + "/" + targetUrl, nil
	}
}
