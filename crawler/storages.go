package crawler

import (
	"github.com/jinzhu/gorm"
)

type ISiteStorage interface {
	SaveIfNotExists(string, interface{}) (bool, error)
	LoadLinksNotExtracted(int) ([]string, error)
	TurnOnIsExtracted([]string) error
}

type IFeatureStorage interface {
	SaveIfNotExists(string, interface{}) (bool, error)
}

// db
type DBStorage struct {
	DB        *gorm.DB
	TableName string
}

func (s *DBStorage) SaveIfNotExists(url string, record interface{}) (bool, error) {
	var err error
	var count int
	tx := s.DB.Begin()

	if err = tx.Table(s.TableName).Where(URL_FIELD+"= ?", url).Count(&count).Error; err != nil {
		tx.Rollback()
		return false, err
	}
	if count > 0 {
		tx.Rollback()
		return false, nil
	}
	if err = tx.Table(s.TableName).Create(record).Error; err != nil {
		tx.Rollback()
		return false, err
	}
	return true, tx.Commit().Error
}

func (s *DBStorage) LoadLinksNotExtracted(limit int) ([]string, error) {
	var err error
	records := []SiteModel{}
	if err = s.DB.
		Table(s.TableName).
		Where(IS_EXTRACTED_FIELD+" = ?", false).
		Limit(limit).
		Find(&records).Error; err != nil {
		return nil, err
	}

	var links []string
	for _, v := range records {
		links = append(links, v.GetUrl())
	}
	return links, nil
}

func (s *DBStorage) TurnOnIsExtracted(links []string) error {
	var err error
	if err = s.DB.
		Table(s.TableName).
		Where(URL_FIELD+" IN (?)", links).
		Update(IS_EXTRACTED_FIELD, true).Error; err != nil {
		return err
	}
	return nil
}
