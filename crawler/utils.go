package crawler

import (
	"reflect"
	"strings"
	"sync"
)

func Contains(slice []string, target string) bool {
	for _, v := range slice {
		if v == target {
			return true
		}
	}
	return false
}

func FilterMap(data map[string]interface{}, keys []string, toLower bool) map[string]interface{} {
	result := make(map[string]interface{})

	for k, v := range data {
		if toLower {
			k = strings.ToLower(k)
		}
		for _, kt := range keys {
			if k == kt {
				result[k] = v
			}
		}
	}

	return result
}

func StructToMap(data interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	elem := reflect.ValueOf(data).Elem()
	size := elem.NumField()

	for i := 0; i < size; i++ {
		field := elem.Type().Field(i).Name
		value := elem.Field(i).Interface()
		result[field] = value
	}

	return result
}

func LowerKeys(data map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for k, v := range data {
		result[strings.ToLower(k)] = v
	}
	return result
}

type SizedWaitGroup struct {
	count int
	wg    *sync.WaitGroup
	ch    chan struct{}
}

func (s *SizedWaitGroup) GetCount() int {
	return s.count
}

func (s *SizedWaitGroup) Add() {
	s.count++
	s.wg.Add(1)
	s.ch <- struct{}{}
}

func (s *SizedWaitGroup) Done() {
	s.count--
	s.wg.Done()
	<-s.ch
}

func (s *SizedWaitGroup) Wait() {
	s.wg.Wait()
}

func NewSizedWaitGroup(limit int) *SizedWaitGroup {
	wg := &sync.WaitGroup{}
	return &SizedWaitGroup{
		wg: wg,
		ch: make(chan struct{}, limit),
	}
}
