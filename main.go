package main

import (
	"flag"
	"fmt"

	"github.com/company1101/gxxgle/app/models"

	"github.com/company1101/gxxgle/crawler"
)

func main() {
	s := flag.String("starturl", "https://www.reecekenney.com/", "url to start from")
	n := flag.Int("num", 100, "num insert sites")
	l := flag.Int("loglevel", 3, "ERROR: 3+, WARN: 2+, INFO: 1+, NOTHING: 0")
	r := flag.Int("limitroutine", 10, "max simultaneous routines")
	t := flag.Bool("droptable", false, "drop table or not")
	m := flag.String("mode", "dev", "dev or prod")
	flag.Parse()

	cnn, err := crawler.ConnectDb("./conf/database/" + *m + ".toml")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer cnn.Close()

	dropTable := *t
	if dropTable {
		cnn.DropTableIfExists("sites")
		cnn.DropTableIfExists("images")
	}
	if !cnn.HasTable("sites") {
		cnn.CreateTable(&models.Site{})

	}
	if !cnn.HasTable("images") {
		cnn.CreateTable(&models.Image{})
	}

	site := crawler.Site{
		Identifier: "sites",
		Parser:     &models.MyParseAllAnchors{},
		Storage:    &crawler.DBStorage{DB: cnn, TableName: "sites"},
	}
	image := crawler.Feature{
		Identifier: "images",
		Parser:     &models.MyImageParser{},
		Storage:    &crawler.DBStorage{DB: cnn, TableName: "images"},
	}

	startUrl := *s
	numUrls := *n
	logLevel := *l
	limitRoutine := *r
	limitLoadOnce := 100
	locale := "ja"
	ignoreHosts := []string{
		"https://twitter.com",
		"https://www.google.com",
		"https://www.facebook.com",
		"http://www.facebook.com",
		"https://plus.google.com",
		"http://plus.google.com",
	}
	fmt.Printf("numUrls: %d logLevel: %d limitRoutine: %d dropTable: %t\n",
		numUrls, logLevel, limitRoutine, dropTable)

	crawler.Crawl(
		startUrl,
		locale,
		numUrls,
		ignoreHosts,
		site,
		[]crawler.Feature{image},
		limitRoutine,
		limitLoadOnce,
		logLevel,
	)
}
