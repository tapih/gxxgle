package controllers

import (
	"net/http"

	"github.com/company1101/gxxgle/app/models"
	"github.com/company1101/gxxgle/app/utils"
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

// helper
var tableCandidates []string = []string{models.SiteTable, models.ImageTable}

func contains(candidates []string, target string) bool {
	for _, v := range candidates {
		if target == v {
			return true
		}
	}
	return false
}

func makePageIdx(page, count, pageSize int) []int {
	var pageIdx []int
	numPages := utils.DivideWithCeil(count, pageSize)
	maxPageToShow := 10
	pageFrom := utils.Max(page-maxPageToShow/2, 1)
	for i := 0; i < maxPageToShow; i++ {
		if currPage := pageFrom + i; currPage > numPages {
			break
		}
		pageIdx = append(pageIdx, pageFrom+i)
	}
	return pageIdx
}

// main
func (c App) Index() revel.Result {
	return c.Render()
}

func (c App) Search(term string, page int, mode string) revel.Result {
	// default value
	if page == 0 {
		page = 1
	}
	if !contains(tableCandidates, mode) {
		mode = models.SiteTable
	}

	pageTitle := term + " - Gxxgle Search"

	// TODO error handling
	if mode == models.ImageTable {
		pageSize := revel.Config.IntDefault("image.pagesize", 50)
		count, _ := models.CountImages(term)
		results, _ := models.SearchImages(term, page, pageSize)
		pageIdx := makePageIdx(page, count, pageSize)
		return c.Render(pageTitle, term, mode, count, results, page, pageIdx)
	} else {
		pageSize := revel.Config.IntDefault("site.pagesize", 20)
		count, _ := models.CountSites(term)
		results, _ := models.SearchSites(term, page, pageSize)
		pageIdx := makePageIdx(page, count, pageSize)
		return c.Render(pageTitle, term, mode, count, results, page, pageIdx)
	}
}

func (c App) IncrementVisited(url string, mode string) revel.Result {
	var err error
	if mode == models.ImageTable {
		err = models.IncrementImageClicked(url)
	} else {
		err = models.IncrementSiteClicked(url)
	}
	if err != nil {
		c.Response.Status = http.StatusInternalServerError
	}
	data := make(map[string]interface{})
	return c.RenderJSON(data)
}
