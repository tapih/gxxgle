package models

import (
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/company1101/gxxgle/app"
	"github.com/company1101/gxxgle/crawler"
	"github.com/jinzhu/gorm"
)

type Site struct {
	ID uint64 `gorm:"primary_key"`
	crawler.SiteModel
	Title       string `gorm:"type:varchar(512)"`
	Description string `gorm:"type:varchar(2048)"`
	Keywords    string `gorm:"type:varchar(512)"`
	Clicked     int    `gorm:"default:0"`
}

var SiteTable string = "sites"

func searchSitesCore(term string) *gorm.DB {
	wc := "%" + term + "%"
	return app.DB.Table(SiteTable).Where(
		"title LIKE ? OR url LIKE ? OR keywords LIKE ? OR title LIKE ?",
		wc, wc, wc, wc)
}

func CountSites(term string) (int, error) {
	var count int
	err := searchSitesCore(term).Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}

func SearchSites(term string, page int, pageSize int) ([]Site, error) {
	var sites []Site
	err := searchSitesCore(term).Offset((page - 1) * pageSize).Limit(pageSize).Order("clicked desc").Find(&sites).Error
	if err != nil {
		return sites, err
	}
	return sites, nil
}

func IncrementSiteClicked(url string) error {
	var site Site
	var err error
	err = app.DB.Table(SiteTable).Where(crawler.URL_FIELD+" = ?", url).First(&site).Error
	if err != nil {
		return err
	}
	err = app.DB.Table(SiteTable).Model(&site).Update("clicked", site.Clicked+1).Error
	if err != nil {
		return err
	}
	return nil
}

type TitleNotFound struct {
	url string
}

func (e TitleNotFound) Error() string {
	return fmt.Sprintf("Title not found in %s", e.url)
}

type MyParseAllAnchors struct {
	*crawler.ParseAllAnchors
}

func (p *MyParseAllAnchors) Extract(doc *goquery.Document, url string) (crawler.ISiteModel, error) {
	rec := &Site{}

	// title
	sel := doc.Find("title").First()
	v, err := sel.Html()
	if err != nil || v == "" {
		return rec, TitleNotFound{url}
	}
	rec.Title = strings.Replace(v, "\n", " ", -1)
	// meta
	doc.Find("meta").Each(func(_ int, s *goquery.Selection) {
		v, exists := s.Attr("name")
		if !exists {
			return
		}

		switch v {
		case "description":
			c, exists := s.Attr("content")
			if exists {
				rec.Description = c
			}
		case "keywords":
			c, exists := s.Attr("content")
			if exists {
				rec.Keywords = c
			}
		}
	})

	// others
	rec.Url = url
	rec.IsLinkExtracted = false
	rec.Clicked = 0
	return rec, nil
}
