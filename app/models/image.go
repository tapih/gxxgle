package models

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/company1101/gxxgle/app"
	"github.com/company1101/gxxgle/crawler"
	"github.com/jinzhu/gorm"
)

type Image struct {
	ID uint64 `gorm:"primary_key"`
	crawler.FeatureModel
	Site    string `gorm:"type:varchar(1024)"`
	Alt     string `gorm:"type:varchar(512)"`
	Title   string `gorm:"type:varchar(512)"`
	Clicked int    `gorm:"default:0"`
}

var ImageTable string = "images"

func searchImagesCore(term string) *gorm.DB {
	wc := "%" + term + "%"
	return app.DB.Table(ImageTable).Where("title LIKE ? OR alt LIKE ?", wc, wc)
}

func CountImages(term string) (int, error) {
	var count int
	err := searchImagesCore(term).Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}

func SearchImages(term string, page int, pageSize int) ([]Image, error) {
	var images []Image
	err := searchImagesCore(term).Offset((page - 1) * pageSize).Limit(pageSize).Order("clicked desc").Find(&images).Error
	if err != nil {
		return images, err
	}
	return images, nil
}

func IncrementImageClicked(url string) error {
	var image Image
	var err error
	err = app.DB.Table(ImageTable).Where(crawler.URL_FIELD+" = ?", url).First(&image).Error
	if err != nil {
		return err
	}
	err = app.DB.Table(ImageTable).Model(&image).Update("clicked", image.Clicked+1).Error
	if err != nil {
		return err
	}
	return nil
}

type MyImageParser struct{}

func (p *MyImageParser) Extract(doc *goquery.Document, url string) ([]crawler.IFeatureModel, error) {
	var imgs []crawler.IFeatureModel
	doc.Find("img").Each(func(_ int, s *goquery.Selection) {
		alt, altExists := s.Attr("alt")
		title, titleExists := s.Attr("title")
		src, srcExists := s.Attr("src")
		u, err := crawler.MakeAbsoluteUrl(src, url)

		if !altExists && !titleExists || !srcExists || err != nil {
			return
		}

		// correct: url -> Site, u -> Url
		// incorrect: u -> Site, url -> Url
		img := &Image{
			FeatureModel: crawler.FeatureModel{Url: u}, Site: url, Alt: alt, Title: title}
		imgs = append(imgs, img)
	})
	return imgs, nil
}
