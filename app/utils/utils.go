package utils

func Max(x, y int) int {
	if x < y {
		return y
	} else {
		return x
	}
}

func Min(x, y int) int {
	if x > y {
		return y
	} else {
		return x
	}
}

func DivideWithCeil(x, y int) int {
	z := x / y
	if x%y > 0 {
		z += 1
	}
	return z
}
