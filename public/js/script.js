$(document).ready(function($) {
    // click
    $('.js__click-count').on('click', function() {
        var url = $(this).attr('href');
        var searchMode = $(this).attr('data-mode');
        if (!url) {
            console.error('url not found');
            return;
        }
        incrementClicked(url, searchMode, true);
        return false;
    });

    // masonry
    var grid = $('.grid');
    if (grid.length) {
        $('.found-image__img-link').each(function(index, elem) {
            loadImage(grid, elem);
        });
    }

    // fancybox
    var box = $('[data-fancybox]');
    if (box.length) {
        box.fancybox({
            caption: function(instance, item) {
                var caption = $(this).data('caption') || '';
                var site = $(this).data('site') || '';

                if (item.type === 'image') {
                    caption =
                        (caption.length ? caption + '<br>' : '') +
                        '<a href="' +
                        item.src +
                        '">View image</a><br>' +
                        '<a href="' +
                        site +
                        '">View page</a>';
                }

                return caption;
            },
            afterShow: function(instance, item) {
                // 注意: ここでcaptionと同じことをしてもundefinedになる…
                // console.log($(this).data('site')); // undefined
                if (item.type === 'image') {
                    incrementClicked(item.src, 'images');
                }
            },
        });
    }
});

// to avoid overwrap
var timer;
function loadImage(grid, elem) {
    var img = $('<img>');
    img.on('load', function() {
        $(elem).prepend(img);
        clearTimeout(timer);
        timer = setTimeout(function() {
            $('.found-image__img-link > img').css('visibility', 'visible');
            $('.found-image__img-link').each(function(index, elem) {
                var imgLoaded = $(elem).find('img')[0];
                if (!imgLoaded) {
                    return
                }
                var size = $(elem).find('.found-image__size');
                size[0].innerText = imgLoaded.naturalHeight + ' x ' + imgLoaded.naturalWidth;
            });
        }, 500);
    });
    img.on('error', function() {
        $('.' + $(elem).attr('data-parent')).remove();
    });
    img.attr('src', $(elem).attr('data-url'));
    img.attr('alt', $(elem).attr('data-alt'));
}

function incrementClicked(url, searchMode, jumpToLink) {
    // TODO 直接POSTされると検索上位になる->PageRank
    $.post('/increment', { url: url, mode: searchMode })
        .done(function() {
            if (jumpToLink) {
                window.location.href = url;
            }
        })
        .fail(function(jqXHR) {
            console.error('increment failed ' + jqXHR.status);
        });
}
